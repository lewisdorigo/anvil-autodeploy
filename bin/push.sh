BRANCH="master"
DIRNAME="/var/www/public_html"
COMPOSER="false"

while [[ $# -gt 1 ]]
    do

    PARAM="$1"
    VALUE="$2"

    case $PARAM in
        -b | --branch)
            BRANCH=$VALUE
            ;;
        -p | --path)
            DIRNAME=$VALUE
            ;;
        -c | --composer)
            COMPOSER=$VALUE
            ;;
    esac
    shift
done

echo " "
echo "Deploying "$BRANCH" to \`"$DIRNAME"\`."

cd "$DIRNAME"
git pull origin "$BRANCH"

if [ "$COMPOSER" != "false" ]; then
    echo " "
    echo 'Running composer install…'
    composer install
fi