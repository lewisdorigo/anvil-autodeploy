DIRNAME="/var/www/public_html"

while [[ $# -gt 1 ]]
    do

    PARAM="$1"
    VALUE="$2"

    case $PARAM in
        -p | --path)
            DIRNAME=$VALUE
            ;;
    esac
    shift
done

echo " "
echo "Flushing redirects in \`$DIRNAME\`."

cd "$DIRNAME"

echo " "
echo "Removing old redirects map…"
rm redirects.map*


echo "Creating new redirect map…"
httxt2dbm -i redirects.txt -o redirects.map

echo "Restarting apache…"
sudo service apache2 restart



echo " "
echo "Redirects Cleared."
